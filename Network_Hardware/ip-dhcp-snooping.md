---
title: IP DHCP Snooping
description: 
published: true
date: 2020-11-13T00:36:46.587Z
tags: ip, dhcp, snooping, prevent, rogue
editor: markdown
dateCreated: 2020-11-13T00:36:46.587Z
---

# IP DHCP Snooping

The goal of `ip dhcp snooping` is to prevent rogue dhcp servers from existing on your network. `ip helper-address` will not fix this issue, in case you thought it might.


## Basic Config

```
config t

# Enable dhcp snooping
ip dhcp snooping

# Enable Vlans you wish to monitor
ip dhcp snooping vlan 1
ip dhcp snooping vlan 35

# Configure uplink / dhcp server
interface gigabitEthernet 1/0/1 
ip dhcp snooping trust

# If you don't trust the downlinks you need to run the following
no ip dhcp snooping information options

```


## References

https://downloads.dell.com/Manuals/all-products/esuprt_ser_stor_net/esuprt_powerconnect/powerconnect-5548_reference%20guide_en-us.pdf

https://i.dell.com/sites/csdocuments/Shared-Content_data-Sheets_Documents/en/Securing-The-Network-Edge.pdf

https://www.dell.com/support/manuals/en-us/force10-s4048-on/s4048_on_9.9.0.0_config_pub-v1/dhcp-snooping?guid=guid-ed34f2db-40dc-4bcb-8b6c-af80630e3f4d&lang=en-us

https://mrncciew.com/2012/12/27/understanding-dhcp-snooping/

