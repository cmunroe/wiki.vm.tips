---
title: Active Directory Deploy
description: 
published: true
date: 2021-08-26T23:01:52.941Z
tags: 
editor: markdown
dateCreated: 2021-08-26T21:22:19.372Z
---

# Active Directory 2019

Here is a quick guide on getting active directory deployed on Windows Server 2019.

## Pre

Get an updated, fully running Windows 2019 instance.

Drivers:

https://docs.microsoft.com/en-us/windows-server/administration/server-core/server-core-manage#add-hardware-and-manage-drivers-locally

Configure Name:

`sconfig`

Set IP Address:

`Get-NetAdapter` Grab the interface index for your interface.

`New-NetIPAddress -InterfaceIndex # -IPAddress 192.168.1.2 -PrefixLength 24 - DefaultGateway 192.168.1.1`

Set DNS:

`Set-DnsClientServerAddress -InterfaceIndex # -ServerAddresses ("192.168.1.1", "192.168.1.2")`

Install Active Directory:

`Install-WindowsFeature -Name Ad-Domain-Services -IncludManagementTools`

## Fresh Deployement

https://social.technet.microsoft.com/wiki/contents/articles/52765.windows-server-2019-step-by-step-setup-active-directory-environment-using-powershell.aspx#:~:text=Installation%20Steps.%201%20Step%201%3A%20Login%20as%20Local,InterfaceIndex.%205%20Step%205%20%3A%20DNS.%20More%20items



## Join Existing

https://networkproguide.com/add-windows-server-2016-domain-controller-to-existing-domain/#:~:text=At%20the%20Deployment%20Configuration%20screen%20select%20%E2%80%9CAdd%20a,the%20credentials%20to%20use%2C%20and%20then%20click%20Next.

