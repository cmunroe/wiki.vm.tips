---
title: Net Use Command
description: 
published: true
date: 2022-02-08T00:28:41.620Z
tags: 
editor: markdown
dateCreated: 2022-02-08T00:26:21.584Z
---

# Net Use

## Commands

#### help

To get a list of helpful commands for `net use` add a `/?` at the end.

```
net use /?
The syntax of this command is:

NET USE
[devicename | *] [\\computername\sharename[\volume] [password | *]]
        [/USER:[domainname\]username]
        [/USER:[dotted domain name\]username]
        [/USER:[username@dotted domain name]
        [/SMARTCARD]
        [/SAVECRED]
        [/REQUIREINTEGRITY]
        [/REQUIREPRIVACY]
        [/WRITETHROUGH]
        [/TRANSPORT:{TCP | QUIC} [/SKIPCERTCHECK]]
        [/REQUESTCOMPRESSION:{YES | NO}]
        [/GLOBAL]
        [[/DELETE] [/GLOBAL]]]

NET USE {devicename | *} [password | *] /HOME

NET USE [/PERSISTENT:{YES | NO}]
```


#### list attached shares

The `net use` command can be used by itself to list the currently attached network shares you have. This will also state the status.

```
net use
New connections will be remembered.


Status       Local     Remote                              Network

-------------------------------------------------------------------------------
OK           V:        \\fileserver.vm.tips\share-1        Microsoft Windows Network
Disconnected W:        \\fileserver.vm.tips\share-2        Microsoft Windows Network
The command completed successfully.
```

#### connect to share

To connect to a share via command line use `net use \\<server name>\<share> /user:<domain>\<user>` you will then be prompted for the password for the account.


```
net use \\fileserver.vm.tips\share-1 /user:vm\me
Enter the password for 'vm\me' to connect to 'fileserver.vm.tips':
```

#### delete share

To disconnect from a share use `net use \\<server>\<share> /delete`.

```
net use \\fileserver.vm.tips\share-1 /delete
\\fileserver.vm.tips\share-1 was deleted successfully.
```
