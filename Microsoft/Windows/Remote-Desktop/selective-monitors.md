---
title: Selective Monitors for RDP
description: 
published: true
date: 2020-10-13T18:06:35.005Z
tags: 
editor: markdown
dateCreated: 2020-10-13T18:06:35.005Z
---

# Selective Monitors for RDP

Would you like to RDP into a remote desktop connection, but only use some of your monitors? 

Here is how.

## Determine Layout of Screens

In `powershell` run `mstsc.exe /l` to get an overview of your screen layout.

https://i.stack.imgur.com/p4Czk.png

## Edit .rdp file

Open your .rdp file in `notepad` or `vscode`.

Find / add the following lines.

```
use multimon:i:1
selectedmonitors:s:0,1
```


Source: https://superuser.com/questions/472346/remote-desktop-use-two-out-of-four-monitors