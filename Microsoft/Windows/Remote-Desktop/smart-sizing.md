---
title: Smart Sizing
description: 
published: true
date: 2020-11-12T15:58:28.886Z
tags: microsoft, windows, rdp
editor: markdown
dateCreated: 2020-11-12T15:58:28.886Z
---

# Smart Sizing

## One Time

Right click the top of your RDP connection > Select `Smartsizing`.


## Make it Default

Open your `default.rdp` file in your documents using your favorite text editor. Append to the bottom of the file: `smart sizing:i:1`

Save the file, and you should be set. 


### References

https://community.spiceworks.com/topic/1951581-force-a-microsoft-remote-desktop-connection-into-smart-sizing