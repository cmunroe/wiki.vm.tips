---
title: DHCP
description: 
published: true
date: 2023-06-15T22:38:52.876Z
tags: 
editor: markdown
dateCreated: 2023-06-15T22:38:03.418Z
---

# DHCP


## Backup and Restore

1. On your old server, right click and select Backup Configuration.
2. Create folder on desktop and save the config.
3. Grab folder, and copy to your new server. 
4. Move Folder to `%SystemRoot%\System32\DHCP\backup`, otherwise restore option will fail.
5. Install DHCP on new server.
6. Restore, and then select folder in `%SystemRoot%\System32\DHCP\backup`.
7. Authorize server if needed. 


## Automatic Backups.

DHCP automatically takes a backup every 60 minutes and stores in `%SystemRoot%\System32\DHCP\backup`. This can be used in times of crisis. 



## References

1. https://activedirectorypro.com/backup-restore-windows-dhcp-server/