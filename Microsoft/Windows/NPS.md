---
title: NPS
description: Create Logging
published: true
date: 2023-06-15T18:33:00.591Z
tags: 
editor: markdown
dateCreated: 2023-06-15T18:27:59.410Z
---

# NPS

## Backup & Restore

1. Open NPS on old server.
2. Right Click NPS.
3. Export Config. 
4. Save .xml file to desktop.
5. Copy .xml to new server.
6. Open NPS on new server. 
7. Import Config.
8. Register server in Active Directory. Follow Prompts.


## Logging

1. OPEN NPS.
2. Right Click NPS.
3. Properties.
4. Enable Rejected / Successful Authentication requests.
5. Run following commands via admin prompt in cmd. Powershell throws error.

```
auditpol /set /subcategory:”Network Policy Server” /success:enable /failure:enable
auditpol /get /subcategory:”Network Policy Server”
```

6. Validate Logging is configured via the response.