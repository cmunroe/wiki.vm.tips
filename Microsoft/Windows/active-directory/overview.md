---
title: Active Directory
description: 
published: true
date: 2021-08-08T04:20:42.698Z
tags: 
editor: markdown
dateCreated: 2021-08-08T03:57:42.464Z
---

# Active Directory

Based on Active Directory 2012R2.

### Overview

Active Directory uses Kerberose standards for authentication.

- Each new "domain" is called a tree in AD.
- All the trees are a Active Directory "Forest".


### Best Practicices

- Avoid Single Points of Failure
- Time Service
- Use virtualization technology with VMGI
- Use Window Server 2012R2 as Guest or higher.
- Avoid or Disable Snapshots
- Be aware of Security
- Consider taking advantage of cloning in your deployment or recovery strategy
- Start a maximum number of 10 new clones at a time.
- Consider using virtualization technologies.... (Add More Here)

#### Security Issues
- Domain Controllers are a prime target for attacks and the most important resource to secure.
- Network Security
- Authentication Attacks
- Elevation of Privilege
- DoS Attack
- Operating System, Service, or Application Attacks
- Operation Risks
- Physical Security Threats


##### Server Fixes

- Estable Update Management Process
- Increase the security of the communictation protocols:
	- Secure LDAP
  - Secure IPsec
  - SMB Signing
- Secure the Operating System by using
	- Baseline security by using SCW
  - Server Core Install
  - Bitlocker Drive Encryption
  
##### Authentication Fixes
- Secure user accounts and passwords
- Secure groups with elevated permissions
- Audit critical object changes
- Deploy secure authentication, such as smart cards.
- Secure network activity
- Establish deprovisioning and cleanup processes.
- Secure client computer.

##### Secure Physical Access
- RODCs
- BitLocker
- Hot-Swap disk systems can lead to domain controller theft
- Protected virtual disks: Virtual machine admins must be highly trusted.
- Store backups in secure locations.


##### Password Policies
- Enforce Password History
- Max Password Age
- Min Password Age for history
- Min Password Length
- Password Must meet Complex
- Store passwords using
- Account Lockout Threshold
- Account lockout duration
- Kerbose Ticket Lifetimes

### FSMO Roles

FSMO roles do not automatically fail over. 

You can find the Domain Roles under "Operating Masters" for each domain.

- Forest Roles (One Per Forest)
	- Domain Naming Master
  	- Naming Authority for domains to prevent conflicts.
  - Schema Master
  	- Schema Change Controller / Authority

- Domain Roles (One Per Domain)
  - RID
  	- Makes SIDs
  - PDC Emulator
  	- Time master / Time Synchronous
    - Passwords
  - Infastructure
  	- Updates groups when users are moved between domains.



### Install

Upgrades are not suggested.

#### Virtualization Gotchas

- Time Synchronization
	- You should verify that it is getting it's time from a a valid source.
  - Verify you aren't getting your time from the HOST VM OS.
  - Functinal Level 2012 + 2012 Server supports rolling back VMs.
  	- Lower than that, and AD will get murdered.
    - Based on Checkpoints and USN, Update Sequence Number.
  - Compares the stored VM Generation Identifier against the VMGI that is provided by the Hypervisor.
  - Possible to "Clone" an Active Directory server. Server name is a gotcha.
  	- https://www.rebeladmin.com/2015/05/step-by-step-guide-to-clone-a-domain-controller/
    - Requires PowerShell.

#### GUI
1. Server Manager
2. Manage > Add Roles and Features.
3. Select "Active Directory." 
4. Add Feature.
5. Configure AD.

#### Core
Powershell experience is a requirement here.

### Configure AD

#### Deployment Types
- Add to existinging deployment.
- Add a Child domain.
- Add a new "Tree".
- Start a new "Forest" (Root Domain).


## Active Directory CS

PKI and Certificates Provide:

- Confidentiality
- Integrity
- Authenticity
- Non-Repudiation
- Availabilty

Subordinate Certificate Authroity is when you sign via a third party vendor. 

CRL is Certificate Revoked List, CRLs can be huge.

Online Responders help fix CRL slowness, by responding if a certificate is valid. 

### Deployments

- CA
	- Stand-Alone
  - Enterprise
  	-Enterprise intigrates with AD.
- CA Web Enrollment
- Online Responder 
- Network Device Entrollment Service
- Certificate Enrollment Web Service
- Certificate Entrollment Policy Web Service


CAPolicy.inf:

Allows for automation of the installation of CA>

- Certification Practice Statement
- Object Identifier
- CRL Publication INtervals
- CA Rewnewal Settings
- Key Size
- Certificate Validity Period
- CDP and AIA paths


### Moving Root CA

Note: You must use the old CA name as the new CA server name.

1. Record the names of the certificate templates.
2. Back up a CA in the CA admin console.
3. Export the registry subkey
4. UNisntall the CA role.
5. Confirm the %SystemRoot% folder locations.
6. Remove the old CA from the domain.

7. Install the AD CS.
8. Use the existing private key.
9. Restore the registry file.
10. Restore the CA database and settings.
11. Restore the certificate templates.


## Active Directory RMS

Notes:
- User needs to "Secure" the data. 
- Honestly, not a suggested deployment option.
- Super User permission isn't retroactive, should be created at the start.

- Information protection technology
- Designed to reduce data leakage
- integrated with microsoft products and windows operating systems
- proetects data when at rest in transit and in any location.

The primary use for AD RMS is to control the distribution of sensitivie information and typical useage scenarios inclide:
- Preventing access to confidentional infrmation from leaving the organization.
- 
- 
- 

Overview of components:
- AD RMS Cluster:
	- Created when you deploy the first AD RMS server.
- AD RMS Server
	- Licenses AD RMS protected content
  - Certifies the identity of trusted users and devices.
- AD RMS Client:
	- Built into Window Vista and new operating systems
  - Interacts with AD RMS-Enabled applications
-AD RMS-Enabled Applications:
	- Allows publication and consumption of AD RMS protected content.
  - Inclides Microsoft Office 2007+, Exchange Server, and SharePoint server.
  - Can be created by using AD RMS SDKs


AD RMS Certificates and Licenses:
- Server Licensor Certificate
- AD RMS Machine Certificate
- Rights Account Certificate
- Client Licensor Certificate
- Publishing License (External User License $20k to $30k)
- End-Use License (Internetal User License)



## Active Directory FS

- Retain control of passwords.
- Allows 3rd party password authentication / intigrations.
- We are claims provider or Identity Provider.
- Third Party is Application Provider
- 

## Active Directory LDS

### Overview

- Stripped down version of Active Directory
- Would be good for Client side authentication
- Used for applications
- Does not require DNS infastrcuture
- You can run multiple instances of AD LDS on the same machine.
- You can modify AD LDS to meet specific application requirements


## DAC

Dynamic Access Control.

Not being trained in 2016, as it is considered "Deprecated" by Microsoft and very few people use it. 

### Overview

- DAC is a new security mechanism for resource access control in Window Server 2012
- Controls access to Data.
- DAC uses claims and properties together with expressions to control access.
- DAC Provides 
	- Data Classification
  - Access control to files
  - Auditing of file access 
  - Optional roghts management services protection integration.

## Work Place Join

### Overview

- Workplace Join enhances the BYOD concept
- Users can operate their private devices in your AD network.

## Azure Active Directory

Cloud based version of Active Direction hosted by Microsoft.

- Cloud / Hybrid setups possible.
- Supports apps in the cloud.
- Suggest having AD DB / Logs / etc on data disk, and not on OS disk.
- Cloud should be allowed to set the IP via Dynamic.

## FIM / MIM

### Notes

Should look into this with SAP: http://download.microsoft.com/download/5/7/f/57f1490e-8a8d-497b-bbae-ec2a44b3799f/miis_sap.pdf

### Overview

- FIM was the original name, and was renamed MIM.
- Certificate and Smart Card Management
- Automatic Deprovisioning
- Directory Synchronization
	- Sync between you and external app.

- User Provisioning
- User Management:
	- SharePoint-based Portal
  - Automated, Codeless user provision and deprovisioning.
  - Self-Service Management
- Group Management
	- Dynamic Based Groups
  - Rich group management capabilities
  - Expensive. Product you will need to buy.
  


## GPO

You can install templates, and then that gives you access to edit those functions in GPO.

O365 Example: https://www.microsoft.com/en-us/download/details.aspx?id=49030

If you want to see what GPOs are being applied to a computer: `gpresult /r`

## Terms

### Authoritative Restore

Authoritative restore is in NTDSUTIL and can forcefully restore to other domain controllers your current state. 

It does this by adding 10,000 to the update number.


### DSRM

Directory Services Restore Mode.

Used if you ever have to do a restore of active directory. Used to logon to the server if the active directory service is down.

This password be done under NTDSUTIL. 

### Functional Level

This allows you to dumb down the communication between DCs to the lowest common denominator of all DC servers. 

2008 vs 2012R2, you would use 2008 functional level.

Backwards compatibility.

Generally supports the last two major releases.

https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/active-directory-functional-levels#windows-server-2019

### Global Catalog (GC)

Has a list of all users in a "Forest".

### KCC

( See ISTG )

Network Consitenenty Checker

Creates a partner to partner communication in a circle. 

Takes 15 seconds to notify each local partner.

Maximum of 45 seconds in circle from notifying to end of chain before spokes begin being created.

DC1 -> (15sec) -> DC2 -> (15sec) -> DC3 -> (15sec) || Spokes begin to be generated if longer.


### ISTG

( See KCC )

This is the KCC checker for intersite topology changes.

This generally happens at 15 minute internval.

It can be forced to run immedietly. 

### NTDUTIL

Command line utility for working / backing up active directory.

### RODC

Read-Only DC.

Allows for a local Active Directory server, as a read only replica. 


### SAM

Local Authentication on Windows computer.

### Schema

You can add the ability to use the schema tool by `regsvr32 schmmgmt.dll`; However, this is beyond not suggested.

### SID

Each object in Active Directory is given an SID. This SID is a unique string that will be generated for each object by AD. If you recreate an account a new SID will be generated and previous permissions of the same name will not be reinstated. 

Active Directory -> File Server:
- User (SID)  -> File (Permission will use SID instead of username)
  
During Authentication:
- User is given Access Token.
- Access Token Contents:
  - User SID
  - Group SID #1
  - Group SID #2
  - Group SID #N
- Access Token generated on login. 
  - (NOTE) Due to this, if a new group is added you will need the user to reauthenticate.
  
### Tombstone

To prevent AD Replication from imporperly recreating a deleted user, we tombstone the user. Tombstone places a "RIP" flag on the account, and increse the update number on the record and then is propogated to the rest. 

After 180 days the record is garbage collected.

You can restore tombstoned accounts using the recycle bin since 2008. 
  
### Work Folders

A self-hosted OneDrive that allows a user to access data from a mobile app or computer to your corproate infastructure. 