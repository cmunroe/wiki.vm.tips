---
title: Server 2016 Installation
description: A quick summary of Installation
published: true
date: 2020-05-18T22:28:30.852Z
tags: 
---

# Installation

## Select Version

### GUI

Standard 

Datacenter

### Core

Standard 

Datacenter

## System Partition

Windows servers 2016 requires a system partition. 

## IP

Set the ip address of the server, or define that it will be on DHCP.

## Update

Make sure you update windows... duh.

## Name

Name the server....

## Role

Set our role...
