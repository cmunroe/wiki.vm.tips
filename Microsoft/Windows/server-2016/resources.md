---
title: Server 2016 Required Resources
description: A quick summary of Resources
published: true
date: 2020-05-18T22:28:33.454Z
tags: 
---

# Minimum Requirements


|  Resource |      Minumum      |
|:---------:|:-----------------:|
| Processor | 1.4 GHz 64 Bit    |
| Memory    | 512 MB / 2 GB ECC |
| Storage   | 32 GB             |
| Network   | 1 Gigabit         |

# Maximums
|  Resource |   Virtual Max  |  Physical Max  |
|:---------:|:--------------:|:--------------:|
| Processor | 240 Processors | 512 Processors |
|   Memory  |      12 TB     |      24 TB     |