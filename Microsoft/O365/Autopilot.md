---
title: AutoPilot
description: 
published: true
date: 2021-08-11T18:54:26.474Z
tags: microsoft, autopilot, 0365
editor: markdown
dateCreated: 2021-08-11T17:10:33.832Z
---

# Microsoft Autopilot

https://endpoint.microsoft.com/

Devices -> Entrolled Devices -> Windows Enrollment

## Create

Windows Autopilot Deployment Program -> Deployment Profiles

Create Name: This will be group name, so name it something unique to the image and the imaging process.

Next....

Deployment Mode: User Driven 
Join... : Hybrid (most likely)
Skip AD Connectivity Check: No
Microsoft Software License Terms: Hide
Privacy Settings: Hide
Hide change account options: Hide
User Account Type: Standard 
Allow White Glove OCBE: No
Language Region: User Select
Automatically Configure Keyboard: Yes
Apply Device name Template: ... %SERIAL%

Next...

Scope: Default

Next...

Set your Scoped Group.

Save...

### Configure Domain
Device -> Configuration Profile

Computer Name Prefix: `%SERIAL%`
Domain Name: `example.com`
Organization Unit: `computers`




## Enroll Computer

### Getting HWID

The Hardware ID script:
This makes a HWID directory at root C: and puts the .CSV inside. That’s what you need to add to endpoint manager.
(For now run ISE as admin and run this, click yes to everything. I need to make this as an actual script/executable/whatever, but this works for now)

```
md c:\\HWID
Set-Location c:\\HWID
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted
Install-Script -Name Get-WindowsAutoPilotInfo
Get-WindowsAutoPilotInfo.ps1 -OutputFile AutoPilotHWID.csv

```

### Import to Console.

https://endpoing.microsoft.com

1. Devices -> Entrolled Devices -> Windows AUtopilot Deployment Program -> Devices

2. Import... Grab the AutoPilotHWID.CSV file

3. Wait Paitently.

4. Open Devices... Copy Device ID ... Groups -> APOfficeDevices -> Add Member.

5. Devices -> Entrolled Devices -> Windows Autopilot Deployment Program -> Devices

6. Assign User -> Select User. ( User Requires Enterprise Mobility + Security E3)