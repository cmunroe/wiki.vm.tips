---
title: Endpoint Device Upgrades
description: 
published: true
date: 2020-11-05T21:00:39.072Z
tags: microsoft, endpoint, windows, 10, upgrades
editor: markdown
dateCreated: 2020-11-05T20:49:04.215Z
---

# Endpoint Device Upgrades


https://endpoint.microsoft.com/

## Feature Upgrades

`Devices` > `Windows 10 feature updates`

In here you can adjust which release is allowed to be pushed to the paticular group of computers.

This is done by major feature releases.


## Windows 10 update rings

`Devices` > `Windows 10 update rings`

In here you can find, and change how updates are deferred to computers. 

## Groups

`Devices` > `Groups` 

Dyanics groups can be defined under `Dynamic membership rules`.