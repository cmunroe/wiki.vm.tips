---
title: VMWare SNMP v3
description: 
published: true
date: 2023-10-18T23:49:47.850Z
tags: 
editor: markdown
dateCreated: 2023-10-18T23:44:52.955Z
---

# VMWare SNMP v3

This is how to setup VMWare SNMP on ESXI hosts (tested via 8.0)

## Steps

1. First you will need to ssh into the host. You might need to enable SSH service to make this possible.
2. Once logged in, we need to enable SNMP. You can do this via: `esxcli system snmp set -e true`
3. Next, we need to snag some unique engineID value. I will use our mac address for this. We can snag our macs for our server using: `esxcli network nic list`
4. Let's set our EngineID with the following: `esxcli system snmp set -E 01:12:23:34:45:56:67`
5. Now let's set our auth method with: `esxcli system snmp set --authentication SHA1`
6. And then to follow it up we need to set our privacy method: `esxcli system snmp set --privacy AES128`
7. Now we need to generate our hashes. Use the following command: `esxcli system snmp hash --auth-hash [auth] --priv-hash [sec] --raw-secret` Where [auth] is our auth string, and [sec] is our privacy string.
8. Take the hashes generated and create the following string: `esxcli system snmp set -u [user]/[auth-hash]/[priv-hash]/priv` Where [user] is your intended user. [auth-hash] and [priv-hash] should be from the previous command.
9. Test, and make profit. 
  

## Docs

https://docs.vmware.com/en/VMware-vSphere/8.0/vsphere-monitoring-performance/GUID-2E4B0F2A-11D8-4649-AC6C-99F89CE93026.html