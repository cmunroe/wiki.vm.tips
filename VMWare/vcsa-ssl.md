---
title: VCSA SSL
description: 
published: true
date: 2021-03-25T18:10:22.024Z
tags: vmware, vcsa, ssl
editor: markdown
dateCreated: 2020-11-06T00:25:36.752Z
---

# VCSA SSL


## Update SSL on VCSA

1. SSH into the VCSA. You might need to enable the VCSA appliance to allow ssh connections.
2. If you are not at a bash prompt, run the `shell` command.
3. Once you are in the bash prompt, run the following command to begin the process of creating a CSR. `/usr/lib/vmware-vmca/bin/certificate-manager`
4. Selection option 1.
5. Selection option 1 again, to create a CSR. 
6. FIll out the form (Suggested place to store the certificates is /root/)
7. Copy the files locally, then head to your certserver. Paste the vmcsa_issued_csr.csr into the CA website. 
8. Select VMWARE ESXI WEB. 
9. Download the copy BASE 64 Chain, should be a p7b file. 
10. Execute the following command to reform the file as a .pem `openssl pkcs7 -print_certs -in <file>.p7b -out <file>.pem`
	Note: ` You might need to edit the .pem and remove all the comments in the file.`
11. Go to your VCenter web interface > Administration > Certificate Management. 
12. Select Replace, and upload the .pem and .key files to the server. 
13. Reboot server, and verify new certificate is functioning. 


## STS

You will need to regenerate the STS certificate every two years.

To verify how long you have before you need replace the certificate follow the guide below:

https://kb.vmware.com/s/article/79248?lang=en_US&queryTerm=checksts


## References

https://kb.vmware.com/s/article/76719

https://www.vxpert.in/how-to-install-microsoft-ca-signed-certificate-in-vcsa/