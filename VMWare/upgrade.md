---
title: VMware Upgrade
description: A quick summary of Upgrade
published: true
date: 2024-03-15T01:39:02.909Z
tags: 
editor: markdown
dateCreated: 2021-08-08T03:55:11.531Z
---

# VMware Upgrade


## Online
 ### Guide
1. Enable the firewall rule.
```
esxcli network firewall ruleset set -e true -r httpClient
```
2.  Get a list of avilable packages. You can get more info on the right build number by looking at the vmware update sheet on their website. 

ESXi 6.5: https://docs.vmware.com/en/VMware-vSphere/6.5/rn/esxi650-202403001.html
ESXi 6.7: https://docs.vmware.com/en/VMware-vSphere/6.7/rn/esxi670-202403001.html

You can find under the image profiles the correct build to use.
```
esxcli software sources profile list -d https://hostupdate.vmware.com/software/VUM/PRODUCTION/main/vmw-depot-index.xml | grep -i ESXi-6
```
3.  Turn off VMs and put the instance into Maitenance Mode.
4.  Apply updates.

6.5:
```
esxcli software profile update -p ESXi-6.5.0-20240304001-no-tools -d https://hostupdate.vmware.com/software/VUM/PRODUCTION/main/vmw-depot-index.xml
```
6.7:
```
esxcli software profile update -p ESXi-6.7.0-20240304001-no-tools -d https://hostupdate.vmware.com/software/VUM/PRODUCTION/main/vmw-depot-index.xml
```
5.  Verify update.
```
Update Result
     Message: The update completed successfully, but...
```
6.  Remove Maitenance Mode.
7.  Re-apply firewall ruleset.
```
esxcli network firewall ruleset set -e false -r httpClient
```
8.  Reboot server.
9.  Start remaining VMs.

### Ansible

Upgrading a server with an Ansible-Playbook.

`ansible-playbook upgrade-vmware.yml --ask-pass -e version=ESXi-6.7.0-20201103001-no-tools -e targets=<server>`

```

---
- hosts: "{{ targets }}"
  become: yes
  remote_user: root
  tasks:
    - name: Shutdown VMs
      shell: for VE in $(vim-cmd vmsvc/getallvms | awk -F ' *' '$1 ~ /^[0-9]+$/ {print $1}'); do vim-cmd vmsvc/power.shutdown $VE; done
      ignore_errors: yes
    - name: Disable HTTP Firewall
      shell: esxcli network firewall ruleset set -e true -r httpClient
    - name: Install Upgrade
      shell: esxcli software profile update -p {{ version }} -d https://hostupdate.vmware.com/software/VUM/PRODUCTION/main/vmw-depot-index.xml
    - name: Enable HTTP Firewall
      shell: esxcli network firewall ruleset set -e false -r httpClient
    - name: Reboot Server
      shell: reboot
```


### Problem Resolution

If you have issues applying the updates, things to try:

* Use the -no-tools version if it says it has run out of space.
* Enable swap in VMware, and point it at a datastore.

### Resources
[video](https://www.youtube.com/watch?v=Xkh05Wv7D3U){.youtube}
https://www.vladan.fr/how-to-upgrade-esxi-6-0-to-6-5-via-cli-on-line/