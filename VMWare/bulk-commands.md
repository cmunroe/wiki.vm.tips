---
title: VMWare Bulk Commands
description: 
published: true
date: 2020-11-05T16:36:28.445Z
tags: vmware, bulk, commands, cli
editor: markdown
dateCreated: 2020-11-05T16:36:28.445Z
---

# VMWare Bulk Commands


## Bulk Shutdown VMs

`for VE in $(vim-cmd vmsvc/getallvms | awk -F ' *' '$1 ~ /^[0-9]+$/ {print $1}'); do vim-cmd vmsvc/power.shutdown $VE; done`