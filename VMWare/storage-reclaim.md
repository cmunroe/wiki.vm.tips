---
title: VMWare Reclaim
description: 
published: true
date: 2020-11-05T00:31:36.763Z
tags: vmware, reclaim, cli
editor: markdown
dateCreated: 2020-11-05T00:31:03.462Z
---

# VMWare Reclaim

## Get Current Setting

`esxcli storage vmfs reclaim config get --volume-label datastore1`
Resonse:
```
   Reclaim Granularity: 1048576 Bytes
   Reclaim Priority: low
```

## Set Reclaim

Disable: `esxcli storage vmfs reclaim config set --volume-label datastore1 --reclaim-priority none`

Enable: `esxcli storage vmfs reclaim config set --volume-label datastore1 --reclaim-priority low`



### References

https://www.codyhosterman.com/2016/11/whats-new-in-esxi-6-5-storage-part-i-unmap/

