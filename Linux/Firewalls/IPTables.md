---
title: Iptables
description: A quick summary of Iptables
published: true
date: 2020-05-18T22:52:20.254Z
tags: 
---

# IPTables

IPtables is an extremely common firewall on Linux Operating Systems.

## List Current Rules
```
iptables -L
```