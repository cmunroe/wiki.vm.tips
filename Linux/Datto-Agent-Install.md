---
title: Datto Agent Install
description: 
published: true
date: 2023-09-13T15:11:32.349Z
tags: 
editor: markdown
dateCreated: 2023-07-20T17:20:39.957Z
---

# Datto Agent Install

This will quickly get the Datto Agent Install setup and configured.


## Firewall Concerns.

Your device should be reachable by the following ports:

* TCP 25567
* TCP 3260
* TCP 3262

[ref #1]

### UFW 

```
ufw allow 25567
ufw allow 3260
ufw allow 3262
```

## Install Agent

```
curl -sS https://cpkg.datto.com/getLinuxAgent.txt | sudo bash
```

If issues, first thing to check would be that headers are installed: 

```
sudo apt-get install linux-headers-$(uname -r)
```


[ref #2]

## References

#1 https://continuity.datto.com/help/Content/kb/unified-continuity/siris-alto-nas/KB204953800.html
#2 https://continuity.datto.com/help/Content/kb/unified-continuity/siris-alto-nas/KB360001066126.htm
#3 https://github.com/datto/dattobd/releases