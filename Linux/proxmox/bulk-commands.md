---
title: Proxmox Bulk Commands
description: 
published: true
date: 2020-05-18T22:27:59.508Z
tags: 
---

# Proxmox Bulk Commands

## Set CPU to Host
```
for i in $(qm list | awk '{if (NR>1) { print $1 }}'); do qm set $i -cpu "host"; done
```