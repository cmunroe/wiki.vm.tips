---
title: Wipe Hard Drive
description: 
published: true
date: 2021-08-08T04:41:09.336Z
tags: 
editor: markdown
dateCreated: 2021-08-08T03:54:46.885Z
---

# Wipe Hard Drive


For a quick down and dirty hard disk wipe via linux the below command will do. What it does is overwrite the disk 7 times with random bits from the random number generator on the system.

```
#!/bin/bash 
for n in `seq 7`; do dd if=/dev/urandom of=/dev/sda bs=8b conv=notrunc; done

```