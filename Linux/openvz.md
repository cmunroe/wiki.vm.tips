---
title: Openvz
description: A quick summary of Openvz
published: true
date: 2020-05-18T22:27:58.162Z
tags: 
---

# OpenVZ

OpenVZ is a virtualization software that shares the host kernel, memory, and cpu. This runs at a higher level than KVM or VMWare. As such this doesn't allow you to do many things that you can easily do on KVM and VMWare, but allows for very efficient use of resouces. 