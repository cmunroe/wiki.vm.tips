---
title: Yum Package Manager
description: A quick summary of Yum
published: true
date: 2020-05-18T22:27:51.490Z
tags: 
---

# Yum

Yum is a linux package manager.

## Repositories

### List Repositories
```
yum repolist
```

### Disable Repository
```
 yum-config-manager --disable <repo>
 ```


## Links

https://access.redhat.com/sites/default/files/attachments/rh_yum_cheatsheet_1214_jcs_print-1.pdf