---
title: Unattended Upgrades
description: A quick summary of Unattended Upgrades
published: true
date: 2020-05-18T22:27:54.214Z
tags: 
---

# Unattended-Upgrades

## Install 
Scripted:
```
wget --no-check-certificate -qO- https://github.com/Munzy/recipes/raw/master/recipes/debian/unattended-upgrades.sh | bash
```

Install:
```
apt install unattended-upgrades
```

## Enable all updates

```
sed '/-updates\|-backports\|Remove-/s#^//##; /Remove-/s#false#true#; /\/\//d; /^$/d; /Black/,/};/d' "/etc/apt/apt.conf.d/50unattended-upgrades" | tee "/etc/apt/apt.conf.d/51unattended-upgrades_on"
```

## Links

https://www.richud.com/wiki/Ubuntu_Enable_Automatic_Updates_Unattended_Upgrades
